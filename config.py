from yacs.config import CfgNode

model_cfg = CfgNode()

model_cfg.lffd_face_detection = CfgNode()
model_cfg.lffd_face_detection.resolution = (240, 240)
model_cfg.lffd_face_detection.output_scales = 5
model_cfg.lffd_face_detection.receptive_field_list = [20, 40, 80, 160, 320]
model_cfg.lffd_face_detection.receptive_field_stride = [4, 8, 16, 32, 64]
model_cfg.lffd_face_detection.receptive_field_center_start = [3, 7, 15, 31, 63]
model_cfg.lffd_face_detection.model_path = "model_zoo/face/lffd_ssd_240240.onnx"
model_cfg.lffd_face_detection.output_tensors = ["conv8_3_bbox", "slice_axis20",
                                                "conv11_3_bbox", "slice_axis21",
                                                "conv14_3_bbox", "slice_axis22",
                                                "conv17_3_bbox", "slice_axis23",
                                                "conv20_3_bbox", "slice_axis24"]

model_cfg.rfb_face_detection = CfgNode()
model_cfg.rfb_face_detection.resolution = (320, 240)
model_cfg.rfb_face_detection.model_path = "model_zoo/face/rfb_ssd_320240.onnx"

model_cfg.simple_lms_detection = CfgNode()
model_cfg.simple_lms_detection.num_points = 68
model_cfg.simple_lms_detection.resolution = (56, 56)
model_cfg.simple_lms_detection.model_path = "model_zoo/landmark/lms_cnn_56_68points.onnx"

model_cfg.high_res_lms_detection = CfgNode()
model_cfg.high_res_lms_detection.num_points = 68
model_cfg.high_res_lms_detection.resolution = (112, 112)
model_cfg.high_res_lms_detection.model_path = "model_zoo/landmark/lms_cnn_112_68points.onnx"

model_cfg.pfld_lms_detection = CfgNode()
model_cfg.pfld_lms_detection.num_points = 98
model_cfg.pfld_lms_detection.resolution = (112, 112)
model_cfg.pfld_lms_detection.reuse_feature = False
model_cfg.pfld_lms_detection.model_path = "model_zoo/landmark/lms_cnn_112_98points.onnx"
