from config import model_cfg
from .face_detection import *
from .facial_landmark import *

__all__ = ["get_face_detector", "get_lms_detector"]


def get_face_detector(model_name):
    if model_name == "lffd_face_detector":
        model = LFFDFaceDetector(model_cfg.lffd_face_detection)
    elif model_name == "rfb_face_detector":
        model = RFBFaceDetector(model_cfg.rfb_face_detection)
    else:
        raise ValueError(f"Invalid model name {model_name}")

    return model


def get_lms_detector(model_name):
    if model_name == "cnn_lms_detector":
        model = SimpleLmsDetector(model_cfg.simple_lms_detection)
    elif model_name == "high_res_lms_detector":
        model = SimpleLmsDetector(model_cfg.high_res_lms_detection)
    elif model_name == "pfld_lms_detector":
        model = PracticalLmsDetector(model_cfg.pfld_lms_detection)
    else:
        raise ValueError(f"Invalid model name {model_name}")
    return model
