import cv2
import numpy as np
import onnxruntime as ort

from libs.utils import nms
from libs.utils.img_utils import pad


class RFBFaceDetector:
    def __init__(self, config):
        options = ort.SessionOptions()
        options.inter_op_num_threads = 1
        options.intra_op_num_threads = 4
        options.execution_mode = ort.ExecutionMode.ORT_SEQUENTIAL
        options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
        options.log_severity_level = 3

        self.ort_session = ort.InferenceSession(config.model_path, options)
        self.input_name = self.ort_session.get_inputs()[0].name
        self.output_names = self.ort_session.get_outputs()[0].name
        self.resolution = config.resolution

    def _preprocess(self, image):
        tensor = cv2.resize(image, self.resolution)
        tensor = tensor - 127.0
        tensor /= 128.0
        tensor = np.expand_dims(np.transpose(tensor, (2, 0, 1)), 0)
        return tensor.astype(np.float32)

    def __call__(self, image, threshold=0.6, nms_threshold=0.3):
        target_size = image.shape[:2]
        tensor = self._preprocess(image)
        scores, boxes = self.ort_session.run(None, {self.input_name: tensor})
        scores, boxes = scores[0], boxes[0]

        mask = scores > threshold
        scores = scores[mask]

        if scores.shape[0] > 0:
            boxes = boxes[mask]
            boxes, scores = nms.hard_nms(boxes, scores, nms_threshold)

            boxes[:, 0] *= target_size[1]
            boxes[:, 1] *= target_size[0]
            boxes[:, 2] *= target_size[1]
            boxes[:, 3] *= target_size[0]

            return boxes.astype(int), scores
        else:
            return np.empty((0, 4)), np.empty(0)


class LFFDFaceDetector:
    def __init__(self, config):
        self.input_size = config.resolution
        self.input_height, self.input_width = config.resolution
        self.receptive_field_list = config.receptive_field_list
        self.receptive_field_stride = config.receptive_field_stride
        self.receptive_field_center_start = config.receptive_field_center_start

        self.output_scales = config.output_scales
        self.output_tensors = config.output_tensors

        self.constant = [i / 2.0 for i in self.receptive_field_list]
        self.net = cv2.dnn.readNetFromONNX(config.model_path)

    def _preprocess(self, image):
        left_pad = 0
        top_pad = 0
        if image.shape[0] / image.shape[1] > self.input_height / self.input_width:
            resize_scale = self.input_height / image.shape[0]
            input_image = cv2.resize(image, (0, 0), fx=resize_scale, fy=resize_scale)
            left_pad = int((self.input_width - input_image.shape[1]) / 2)
        else:
            resize_scale = self.input_width / image.shape[1]
            input_image = cv2.resize(image, (0, 0), fx=resize_scale, fy=resize_scale)
            top_pad = int((self.input_height - input_image.shape[0]) / 2)

        input_image = pad(input_image, (self.input_height, self.input_width))
        return input_image, left_pad, top_pad, resize_scale

    def _postprocess(self, outputs, score_threshold, left_pad, top_pad, resize_scale):
        bbox_collection = []

        for i in range(self.output_scales):
            score_map = np.squeeze(outputs[i * 2 + 1])
            bbox_map = np.squeeze(outputs[i * 2])
            rf_center_xs = np.array(
                [self.receptive_field_center_start[i] + self.receptive_field_stride[i] * x for x in
                 range(score_map.shape[1])])
            rf_center_xs_mat = np.tile(rf_center_xs, [score_map.shape[0], 1])
            rf_center_ys = np.array(
                [self.receptive_field_center_start[i] + self.receptive_field_stride[i] * y for y in
                 range(score_map.shape[0])])
            rf_center_ys_mat = np.tile(rf_center_ys, [score_map.shape[1], 1]).T

            x_lt_mat = rf_center_xs_mat - bbox_map[0, :, :] * self.constant[i]
            y_lt_mat = rf_center_ys_mat - bbox_map[1, :, :] * self.constant[i]
            x_rb_mat = rf_center_xs_mat - bbox_map[2, :, :] * self.constant[i]
            y_rb_mat = rf_center_ys_mat - bbox_map[3, :, :] * self.constant[i]

            x_lt_mat = x_lt_mat
            x_lt_mat[x_lt_mat < 0] = 0
            y_lt_mat[y_lt_mat < 0] = 0
            x_rb_mat[x_rb_mat > self.input_height] = self.input_height
            y_rb_mat[y_rb_mat > self.input_width] = self.input_width

            select_index = np.where(score_map > score_threshold)
            for idx in range(select_index[0].size):
                bbox_collection.append((x_lt_mat[select_index[0][idx], select_index[1][idx]] - left_pad,
                                        y_lt_mat[select_index[0][idx], select_index[1][idx]] - top_pad,
                                        x_rb_mat[select_index[0][idx], select_index[1][idx]] - left_pad,
                                        y_rb_mat[select_index[0][idx], select_index[1][idx]] - top_pad,
                                        score_map[select_index[0][idx], select_index[1][idx]]))

        bbox_collection = sorted(bbox_collection, key=lambda item: item[-1], reverse=True)
        bbox_collection_numpy = np.array(bbox_collection, dtype=np.float32)
        bbox_collection_numpy = bbox_collection_numpy / resize_scale

        return bbox_collection_numpy

    def __call__(self, image, threshold=0.75, nms_threshold=0.45):
        image, left_pad, top_pad, resize_scale = self._preprocess(image)
        self.net.setInput(cv2.dnn.blobFromImage(image, size=(self.input_height, self.input_width)))
        outputs = self.net.forward(self.output_tensors)

        bbox_with_scores = self._postprocess(outputs, threshold, left_pad, top_pad, resize_scale)
        final_boxes = nms.simple_nms(bbox_with_scores, overlap_threshold=0.4)

        if final_boxes.shape[0] > 0:
            return final_boxes[:, :4].astype(int), final_boxes[:, 4]
        else:
            return np.empty((0, 4)), np.empty(0)
