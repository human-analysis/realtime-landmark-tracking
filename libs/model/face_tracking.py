import numpy as np

from libs.model.modules import KalmanBoxTracker, associate_detections_to_tracks


class SortTracker(object):
    def __init__(self, max_age=30, min_hits=3):
        """
        Sets key parameters for SORT
        """
        self.max_age = max_age
        self.min_hits = min_hits
        self.tracks = []
        self.frame_count = 0

    def track_boxes(self, detections):
        self.frame_count += 1
        tracks = np.zeros((len(self.tracks), 5))
        to_del = []
        for t, trk in enumerate(tracks):
            pos = self.tracks[t].predict()
            trk[:] = [pos[0], pos[1], pos[2], pos[3], 0]
            if np.any(np.isnan(pos)):
                to_del.append(t)

        tracks = np.ma.compress_rows(np.ma.masked_invalid(tracks))
        for t in reversed(to_del):
            self.tracks.pop(t)

        matched, unmatched_detections, unmatched_tracks = associate_detections_to_tracks(detections, tracks)
        # update matched tracks with assigned
        for t, trk in enumerate(self.tracks):
            if t not in unmatched_tracks:
                d = matched[np.where(matched[:, 1] == t)[0], 0]
                trk.update(detections[d, :][0])
                trk.confirmed = True
            else:
                trk.confirmed = False

        # create and initialise new tracks for unmatched detections
        for i in unmatched_detections:
            trk = KalmanBoxTracker(detections[i, :])
            self.tracks.append(trk)

        i = len(self.tracks)
        track_boxes, track_ids = [], []
        for trk in reversed(self.tracks):
            d = trk.get_state()  # tlbr
            if (trk.time_since_update < 1) and (trk.hit_streak >= self.min_hits or self.frame_count <= self.min_hits):
                track_boxes.append(d)
                track_ids.append(trk.track_id + 1)
            i -= 1
            # remove dead track let
            if trk.time_since_update > self.max_age:
                self.tracks.pop(i)
        return track_boxes, track_ids

    def update(self, boxes, scores):
        """
        :param boxes: xyxy
        :param scores:
        :return:
        """
        detections = np.empty((boxes.shape[0], 5))
        detections[:, :4] = boxes
        detections[:, 4] = scores

        track_boxes, track_ids = self.track_boxes(detections)
        return track_boxes, track_ids
