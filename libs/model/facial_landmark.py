import cv2
import numpy as np
import onnxruntime as ort

from libs.utils.img_utils import expand_box


class SimpleLmsDetector:
    def __init__(self, config):
        options = ort.SessionOptions()
        options.inter_op_num_threads = 1
        options.intra_op_num_threads = 4
        options.execution_mode = ort.ExecutionMode.ORT_SEQUENTIAL
        options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
        options.log_severity_level = 3

        self.ort_session = ort.InferenceSession(config.model_path, options)
        self.input_name = self.ort_session.get_inputs()[0].name
        self.resolution = config.resolution
        self.num_points = config.num_points

    def _preprocess(self, image, boxes, conf):
        new_boxes = []
        tensors = np.zeros((len(boxes), 3, *self.resolution), dtype=np.float32)

        for i, box in enumerate(boxes):
            sub_img, extended_box = expand_box(box, image)
            sub_img = cv2.resize(sub_img, self.resolution) / 255
            sub_img = sub_img - [0.485, 0.456, 0.406]
            sub_img = sub_img / [0.229, 0.224, 0.225]
            tensors[i, :] = np.transpose(sub_img, (2, 0, 1))
            new_boxes.append(extended_box)

        return tensors, new_boxes

    def __call__(self, image, boxes, conf, return_all=False):
        tensors, new_boxes = self._preprocess(image, boxes, conf)
        outputs = self.ort_session.run(None, {self.input_name: tensors})[0]

        facial_lms = []
        for box, lms in zip(new_boxes, outputs):
            w, h = box[2] - box[0], box[3] - box[1]
            for i in range(0, self.num_points * 2, 2):
                facial_lms.append((lms[i] * w + box[0], lms[i + 1] * h + box[1]))

        if return_all:
            return facial_lms, outputs, new_boxes
        return facial_lms


class PracticalLmsDetector(SimpleLmsDetector):
    def __init__(self, config):
        super().__init__(config)

    def _preprocess(self, image, boxes, conf):
        new_boxes = []
        tensors = np.zeros((len(boxes), 3, *self.resolution), dtype=np.float32)

        for i, box in enumerate(boxes):
            sub_img, extended_box = expand_box(box, image)
            sub_img = cv2.resize(sub_img, self.resolution) / 255
            tensors[i, :] = np.transpose(sub_img, (2, 0, 1))
            new_boxes.append(extended_box)

        return tensors, new_boxes

    def __call__(self, image, boxes, conf, return_all=False):
        return super().__call__(image, boxes, conf, return_all)
