import cv2

from libs.utils.misc import add_file_prefix
from libs.utils.stream import FPS
from libs.utils.visualization import draw_boxes


def landmark_tracking(face_detector, lms_detector, stream, output, max_faces, max_frames=-1, is_show=False):
    frame = stream.get_frame()
    width, height = frame.shape[:2]

    four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    video_writer = cv2.VideoWriter(output, four_cc, 20, (height, width))

    fps = FPS()

    fps.start()
    stream.start()

    frame_counter = 0
    while True:
        ret, frame = stream.read()
        if frame is None:
            break

        boxes, scores = face_detector(frame, threshold=0.45)
        if max_faces > 0:
            top_k = min(len(scores), max_faces)
            indices = scores.argsort()[-top_k:][::-1]
            scores = scores[indices]
            boxes = boxes[indices]

        facial_lms = None
        if len(boxes) > 0:
            facial_lms = lms_detector(frame, boxes, scores)
        frame = draw_boxes(frame, boxes, scores, facial_lms)

        frame_counter += 1
        fps.update()
        fps.stop()

        cv2.putText(frame, f'FPS: {fps.fps()}', (25, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        video_writer.write(frame)

        if is_show:
            cv2.imshow("Image", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        print(f"FPS: {fps.fps()}")
        if frame_counter == max_frames:
            break

    fps.stop()
    video_writer.release()
    stream.stop()
    cv2.destroyAllWindows()

    print("Elapsed time : {:.2f}".format(fps.elapsed()))
    print("Approx. FPS: {:.2f}".format(fps.fps()))
    print("Num frames: {:.2f} - {:.2f}".format(fps.num_frames, frame_counter))


def landmark_tracking_with_tracker(face_detector, face_tracker, lms_detector, stream, output, is_show=False):
    frame = stream.get_frame()
    width, height = frame.shape[:2]
    four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    video_writer = cv2.VideoWriter(output, four_cc, 20, (height, width))

    fps = FPS()
    fps.start()

    stream.start()
    frame_counter, detect_freq = 0, 3
    detect_fg, update_fg = True, True
    while True:
        ret, frame = stream.read()
        if frame is None:
            break

        if frame_counter % detect_freq or detect_fg:
            boxes, scores = face_detector(frame)

            if boxes.shape[0] > 0:
                update_fg, detect_fg = True, False
            else:
                detect_fg = True
        else:
            update_fg, detect_fg = False, False

        frame_counter += 1
        if boxes.shape[0] > 0:
            track_boxes, track_ids = face_tracker.update(boxes, scores)
            if len(track_boxes) > 0:
                facial_lms = lms_detector(frame, track_boxes, scores)
                frame = draw_boxes(frame, track_boxes, scores, facial_lms)

        fps.update()
        fps.stop()

        cv2.putText(frame, f'FPS: {100}', (25, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        video_writer.write(frame)

        if is_show:
            cv2.imshow("image", frame)
        print(f"[FPS]: {fps.fps()}")


def face_detection(face_detector, input_file, is_show=True):
    image = cv2.imread(input_file)
    boxes, scores = face_detector(image)
    print(boxes, scores)

    image = draw_boxes(image, boxes, scores)
    if is_show:
        cv2.imshow("Image", image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    cv2.imwrite(add_file_prefix(input_file, "face_result"), image)


def landmark_detection(face_detector, lms_detector, input_file, is_show=True):
    image = cv2.imread(input_file)
    boxes, scores = face_detector(image)
    facial_lms = lms_detector(image, boxes, scores)

    image = draw_boxes(image, boxes, scores, facial_lms)
    if is_show:
        cv2.imshow("Image", image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    cv2.imwrite(add_file_prefix(input_file, "lms_result"), image)
