"""
Author: Amr Elsersy
email: amrelsersay@gmail.com
-----------------------------------------------------------------------------------
Description: Head Pose Euler Angles(pitch yaw roll) estimation from 2D-3D correspondences landmarks
"""

import cv2
import numpy as np

from libs.utils.img_utils import estimate_camera_matrix


def get_face_model_3D_landmarks():
    landmarks_3D = np.float32([
        [6.825897, 6.760612, 4.402142],  # LEFT_EYEBROW_LEFT,
        [1.330353, 7.122144, 6.903745],  # LEFT_EYEBROW_RIGHT,
        [-1.330353, 7.122144, 6.903745],  # RIGHT_EYEBROW_LEFT,
        [-6.825897, 6.760612, 4.402142],  # RIGHT_EYEBROW_RIGHT,
        [5.311432, 5.485328, 3.987654],  # LEFT_EYE_LEFT,
        [1.789930, 5.393625, 4.413414],  # LEFT_EYE_RIGHT,
        [-1.789930, 5.393625, 4.413414],  # RIGHT_EYE_LEFT,
        [-5.311432, 5.485328, 3.987654],  # RIGHT_EYE_RIGHT,
        [-2.005628, 1.409845, 6.165652],  # NOSE_LEFT,
        [-2.005628, 1.409845, 6.165652],  # NOSE_RIGHT,
        [2.774015, -2.080775, 5.048531],  # MOUTH_LEFT,
        [-2.774015, -2.080775, 5.048531],  # MOUTH_RIGHT,
        [0.000000, -3.116408, 6.097667],  # LOWER_LIP,
        [0.000000, -7.415691, 4.070434],  # CHIN
    ])

    return landmarks_3D


class EulerAngles:
    def __init__(self, img_shape=(112, 112)):
        self.camera_intensive_matrix = estimate_camera_matrix(img_shape)
        self.landmarks_3D = get_face_model_3D_landmarks()

    def euler_angles_from_landmarks(self, landmarks_2D):
        TRACKED_POINTS_MASK = [33, 38, 50, 46, 60, 64, 68, 72, 55, 59, 76, 82, 85, 16]
        landmarks_2D = landmarks_2D[TRACKED_POINTS_MASK]
        _, rvec, tvec = cv2.solvePnP(self.landmarks_3D, landmarks_2D, self.camera_intensive_matrix, distCoeffs=None)
        rotation_matrix, _ = cv2.Rodrigues(rvec)

        euler_angles = cv2.RQDecomp3x3(rotation_matrix)[0]
        return rvec, tvec, euler_angles
