import cv2

import numpy as np
from numba import jit


@jit
def iou(bb_test, bb_gt):
    """
    Computes IUO between two bboxes in the form [x1,y1,x2,y2]
    """
    xx1 = np.maximum(bb_test[0], bb_gt[0])
    yy1 = np.maximum(bb_test[1], bb_gt[1])
    xx2 = np.minimum(bb_test[2], bb_gt[2])
    yy2 = np.minimum(bb_test[3], bb_gt[3])
    w = np.maximum(0., xx2 - xx1)
    h = np.maximum(0., yy2 - yy1)
    wh = w * h
    o = wh / ((bb_test[2] - bb_test[0]) * (bb_test[3] - bb_test[1])
              + (bb_gt[2] - bb_gt[0]) * (bb_gt[3] - bb_gt[1]) - wh)
    return o


def convert_bbox_to_z(bbox):
    w = bbox[2] - bbox[0]
    h = bbox[3] - bbox[1]
    x = bbox[0] + w / 2.
    y = bbox[1] + h / 2.
    s = w * h  # scale is just area
    r = w / float(h + 1e-6)
    return np.array([x, y, s, r]).reshape((4, 1))


def convert_x_to_bbox(x, score=None):
    w = np.sqrt(x[2] * x[3])
    h = x[2] / w

    if score:
        return np.array([x[0] - w / 2., x[1] - h / 2., x[0] + w / 2., x[1] + h / 2., score]).reshape((1, 5))
    return np.array([x[0] - w / 2., x[1] - h / 2., x[0] + w / 2., x[1] + h / 2.]).reshape((1, 4))


def estimate_camera_matrix(img_shape=(112, 112)):
    c_x = img_shape[0] / 2
    c_y = img_shape[1] / 2
    FieldOfView = 60
    focal = c_x / np.tan(np.radians(FieldOfView / 2))

    return np.float32([
        [focal, 0.0, c_x],
        [0.0, focal, c_y],
        [0.0, 0.0, 1.0]
    ])


def expand_box(box, image):
    height, width, _ = image.shape
    x1 = box[0]
    y1 = box[1]
    x2 = box[2]
    y2 = box[3]
    w = x2 - x1 + 1
    h = y2 - y1 + 1
    size = int(max([w, h]) * 1.1)
    cx = x1 + w // 2
    cy = y1 + h // 2
    x1 = cx - size // 2
    x2 = x1 + size
    y1 = cy - size // 2
    y2 = y1 + size
    dx = max(0, -x1)
    dy = max(0, -y1)
    x1 = max(0, int(x1))
    y1 = max(0, int(y1))

    edx = max(0, x2 - width)
    edy = max(0, y2 - height)
    x2 = min(width, int(x2))
    y2 = min(height, int(y2))

    cropped = image[y1:y2, x1:x2, :]
    if dx > 0 or dy > 0 or edx > 0 or edy > 0:
        cropped = cv2.copyMakeBorder(cropped, int(dy), int(edy), int(dx), int(edx), cv2.BORDER_CONSTANT, 0)
    return cropped, [x1, y1, x2, y2]


def pad(image, target_size, mode="center"):
    w, h, _ = image.shape

    if mode == "center":
        top_padding = max((target_size[0] - w) // 2, 0)
        bottom_padding = max(target_size[0] - top_padding - w, 0)

        left_padding = max((target_size[1] - h) // 2, 0)
        right_padding = max(target_size[1] - left_padding - h, 0)

        return cv2.copyMakeBorder(image,
                                  top_padding,
                                  bottom_padding,
                                  left_padding,
                                  right_padding,
                                  cv2.BORDER_CONSTANT,
                                  0)
    elif mode == "top":
        top_padding = 0
        bottom_padding = max(target_size[0] - w, 0)
        left_padding = max((target_size[1] - h) // 2, 0)
        right_padding = max(target_size[1] - left_padding - h, 0)

        return cv2.copyMakeBorder(image,
                                  top_padding,
                                  bottom_padding,
                                  left_padding,
                                  right_padding,
                                  cv2.BORDER_CONSTANT,
                                  0)
    elif mode == "left":
        top_padding = 0
        bottom_padding = max(target_size[0] - w, 0)
        left_padding = 0
        right_padding = max(target_size[1] - left_padding - h, 0)

        return cv2.copyMakeBorder(image,
                                  top_padding,
                                  bottom_padding,
                                  left_padding,
                                  right_padding,
                                  cv2.BORDER_CONSTANT,
                                  0)
