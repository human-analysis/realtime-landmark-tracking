import os


def make_dir(path):
    os.makedirs(path, exist_ok=True)


def add_file_prefix(file, prefix):
    els = file.split(".")
    return f"{els[0]}_{prefix}.{els[1]}"
