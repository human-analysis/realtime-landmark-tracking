import numpy as np


def area_of(left_top, right_bottom):
    hw = np.clip(right_bottom - left_top, 0.0, None)
    return hw[..., 0] * hw[..., 1]


def iou_of(boxes0, boxes1, eps=1e-5):
    overlap_left_top = np.maximum(boxes0[..., :2], boxes1[..., :2])
    overlap_right_bottom = np.minimum(boxes0[..., 2:], boxes1[..., 2:])

    overlap_area = area_of(overlap_left_top, overlap_right_bottom)
    area0 = area_of(boxes0[..., :2], boxes0[..., 2:])
    area1 = area_of(boxes1[..., :2], boxes1[..., 2:])
    return overlap_area / (area0 + area1 - overlap_area + eps)


def simple_nms(boxes, overlap_threshold):
    if boxes.shape[0] == 0:
        return boxes

    if boxes.dtype != np.float32:
        boxes = boxes.astype(np.float32)

    pick = []
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    sc = boxes[:, 4]
    widths = x2 - x1
    heights = y2 - y1

    area = heights * widths
    indices = np.argsort(sc)

    while len(indices) > 0:
        last = len(indices) - 1
        i = indices[last]
        pick.append(i)

        xx1 = np.maximum(x1[i], x1[indices[:last]])
        yy1 = np.maximum(y1[i], y1[indices[:last]])
        xx2 = np.minimum(x2[i], x2[indices[:last]])
        yy2 = np.minimum(y2[i], y2[indices[:last]])

        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)

        overlap = (w * h) / area[indices[:last]]

        indices = np.delete(indices, np.concatenate(([last], np.where(overlap > overlap_threshold)[0])))

    return boxes[pick]


def hard_nms(boxes, scores, iou_threshold):
    keep = []
    indices = np.argsort(scores)

    while len(indices) > 0:
        current = indices[-1]
        keep.append(current)
        if len(indices) == 1:
            break
        current_box = boxes[current, :]
        indices = indices[:-1]
        rest_boxes = boxes[indices, :]
        iou = iou_of(
            rest_boxes,
            np.expand_dims(current_box, axis=0),
        )
        indices = indices[iou <= iou_threshold]

    return boxes[keep], scores[keep]


def soft_nms(boxes, scores, iou_threshold=0.3,
             method='linear', sigma=0.5, score_thr=0.001):
    if method not in ('linear', 'gaussian', 'greedy'):
        raise ValueError('method must be linear, gaussian or greedy')

    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    # expand dets with areas, and the second dimension is
    # x1, y1, x2, y2, area
    boxes = np.concatenate((boxes, areas[:, None]), axis=1)
    # scores

    retained_box = []
    retained_score = []
    while boxes.size > 0:
        max_idx = np.argmax(scores)
        boxes[[0, max_idx], :] = boxes[[max_idx, 0], :]
        scores[[0, max_idx]] = scores[[max_idx, 0]]
        retained_box.append(boxes[0, :-1])
        retained_score.append(scores[0])

        xx1 = np.maximum(boxes[0, 0], boxes[1:, 0])
        yy1 = np.maximum(boxes[0, 1], boxes[1:, 1])
        xx2 = np.minimum(boxes[0, 2], boxes[1:, 2])
        yy2 = np.minimum(boxes[0, 3], boxes[1:, 3])

        w = np.maximum(xx2 - xx1 + 1, 0.0)
        h = np.maximum(yy2 - yy1 + 1, 0.0)
        inter = w * h
        iou = inter / (boxes[0, 4] + boxes[1:, 4] - inter)

        if method == 'linear':
            weight = np.ones_like(iou)
            weight[iou > iou_threshold] -= iou[iou > iou_threshold]
        elif method == 'gaussian':
            weight = np.exp(-(iou * iou) / sigma)
        else:  # traditional nms
            weight = np.ones_like(iou)
            weight[iou > iou_threshold] = 0

        scores[1:] *= weight
        retained_idx = np.where(scores[1:] >= score_thr)[0]
        boxes = boxes[retained_idx + 1, :]
        scores = scores[retained_idx + 1]

    return np.vstack(retained_box), np.array(retained_score)
