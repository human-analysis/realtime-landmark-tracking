import datetime
import os.path
import queue
from threading import Thread

import cv2


class FPS:
    def __init__(self):
        self._start = None
        self._end = None
        self.num_frames = 0

    def start(self):
        self._start = datetime.datetime.now()

    def get_start(self):
        return self._start

    def stop(self):
        self._end = datetime.datetime.now()

    def update(self):
        self.num_frames += 1

    def elapsed(self):
        return (self._end - self._start).total_seconds()

    def fps(self):
        return self.num_frames / self.elapsed()


class OfflineStream:
    def __init__(self, src):
        if os.path.isfile(src):
            self.stream = cv2.VideoCapture(src)
        else:
            raise ValueError("Invalid stream input")

        grabbed, self.frame = self.stream.read()
        self.stopped = False
        self.q = queue.Queue()
        self.q.put(self.frame)

    def get_frame(self):
        return self.frame

    def start(self):
        Thread(target=self.update, args=()).start()

    def update(self):
        while True:
            if self.stopped:
                return
            grabbed, frame = self.stream.read()
            if grabbed == -1:
                self.stop()
                continue
            self.q.put(frame)

    def read(self):
        return -1, self.q.get()

    def stop(self):
        self.stopped = True
        self.stream.release()

    def is_stop(self):
        return self.q.empty()


class WebCamVideoStream:
    def __init__(self, src=0):
        self.stream = cv2.VideoCapture(src)
        self.grabbed, self.frame = self.stream.read()
        self.stopped = False

    def get_frame(self):
        return self.frame

    def start(self):
        Thread(target=self.update, args=()).start()

    def update(self):
        while True:
            if self.stopped:
                return
            self.grabbed, self.frame = self.stream.read()

    def read(self):
        # return the frame most recently read
        return -1, self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True
        self.stream.release()


class OnlineStream:
    def __init__(self, src):
        self.stream = cv2.VideoCapture(src)
        self.frame_counter = 0

        _, self.frame = self.stream.read()

    def get_frame(self):
        return self.frame

    def start(self):
        pass

    def read(self):
        grabbed, frame = self.stream.read()

        if grabbed == -1:
            return -1, None

        frame_id = -1
        self.frame_counter += 1

        return frame_id, frame

    def stop(self):
        self.stream.release()


def get_stream(source=None, is_online=False, is_camera=False):
    if is_camera:
        stream = WebCamVideoStream()
    elif not is_online:
        stream = OfflineStream(source)
    else:
        stream = OnlineStream(source)
    return stream
