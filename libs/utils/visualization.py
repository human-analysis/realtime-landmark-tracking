import cv2
import numpy as np

from libs.utils.img_utils import estimate_camera_matrix


def draw_euler_angles(frame, box, axis_pts, euler_angles):
    x, y, x1, y1 = box
    w, h = x1 - x, y1 - y
    top_left = (x, y)
    center = (int(x + w // 2), int(y + h // 2))
    x, y = int(x), int(y)

    axis_pts = axis_pts.astype(np.int32)
    pitch_point = list(axis_pts[0].ravel() + top_left)
    yaw_point = list(axis_pts[1].ravel() + top_left)
    roll_point = list(axis_pts[2].ravel() + top_left)

    pitch_point[0], pitch_point[1] = int(pitch_point[0]), int(pitch_point[1])
    yaw_point[0], yaw_point[1] = int(yaw_point[0]), int(yaw_point[1])
    roll_point[0], roll_point[1] = int(roll_point[0]), int(roll_point[1])

    width = 2
    cv2.line(frame, center, tuple(pitch_point), (0, 255, 0), width)  # green Pitch
    cv2.line(frame, center, tuple(yaw_point), (255, 0, 0), width)  # red Yaw
    cv2.line(frame, center, tuple(roll_point), (0, 0, 255), width)  # blue Roll

    pitch, yaw, roll = euler_angles
    cv2.putText(frame, "Pitch:{:.2f}".format(pitch), (x, y - 10), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
    cv2.putText(frame, "Yaw:{:.2f}".format(yaw), (x, y - 25), cv2.FONT_HERSHEY_PLAIN, 1, (255, 0, 0))
    cv2.putText(frame, "Roll:{:.2f}".format(roll), (x, y - 40), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255))

    return frame


def draw_boxes(frame, boxes, scores, lms=None, head_poses=None):
    for i, box in enumerate(boxes):
        box = boxes[i]
        cv2.rectangle(frame, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (0, 255, 0), 1)
        cv2.rectangle(frame, (int(box[0]), int(box[1])), (int(box[2]), int(box[1] + 5)), (0, 255, 0), -1)
        cv2.putText(frame, "{:.2f}".format(scores[i]),
                    (int(box[0]), int(box[1] + 5)), cv2.QT_FONT_NORMAL, 0.2,
                    (40, 40, 40), 1)
        if lms is not None:
            for point in lms:
                cv2.circle(frame, (int(point[0]), int(point[1])), 2, (0, 255, 0), -1)

        if head_poses is not None:
            vis_rvec, vis_tvec, euler_angles = head_poses[i]
            axis = np.identity(3) * 7
            axis[2, 2] = 4
            axis_pts = cv2.projectPoints(axis, vis_rvec, vis_tvec, estimate_camera_matrix(), None)[0]
            frame = draw_euler_angles(frame, box, axis_pts, euler_angles)
    return frame
