import os.path

import click

from libs import ops
from libs.model import get_face_detector, get_lms_detector
from libs.utils.misc import add_file_prefix
from libs.utils.stream import get_stream


@click.command()
@click.option("--face_detector_name", default="lffd_face_detector", type=str)
@click.option("--lms_detector_name", default="pfld_lms_detector", type=str)
@click.option("--input_file", required=True, type=str)
@click.option("--max_faces", default=-1, type=int)
@click.option("--max_frames", default=1000, type=int)
@click.option("--is_online", default=True, type=bool)
@click.option("--is_show", default=False, type=bool)
def track_landmark(face_detector_name, lms_detector_name, input_file, max_faces, max_frames, is_online, is_show):
    face_detector = get_face_detector(face_detector_name)
    lms_detector = get_lms_detector(lms_detector_name)

    if not os.path.isfile(input_file):
        raise ValueError("File is not exists")

    stream = get_stream(input_file, is_online)
    ops.landmark_tracking(face_detector, lms_detector, stream,
                          add_file_prefix(input_file, "lms_tracking"), max_faces, max_frames, is_show)


@click.command()
@click.option("--face_detector_name", default="lffd_face_detector", type=str)
@click.option("--lms_detector_name", default="pfld_lms_detector", type=str)
@click.option("--max_faces", default=-1, type=int)
def track_landmark_camera(face_detector_name, lms_detector_name, max_faces):
    face_detector = get_face_detector(face_detector_name)
    lms_detector = get_lms_detector(lms_detector_name)

    stream = get_stream(is_camera=True)
    ops.landmark_tracking(face_detector, lms_detector, stream,
                          add_file_prefix("data/video/camera.mp4", "lms_tracking"), max_faces, is_show=True)


@click.command()
@click.option("--face_detector_name", default="lffd_face_detector", type=str)
@click.option("--lms_detector_name", default="pfld_lms_detector", type=str)
@click.option("--input_file", required=True, type=str)
def predict_landmark(face_detector_name, lms_detector_name, input_file):
    face_detector = get_face_detector(face_detector_name)
    lms_detector = get_lms_detector(lms_detector_name)
    ops.landmark_detection(face_detector, lms_detector, input_file)


@click.command()
@click.option("--face_detector_name", default="lffd_face_detector", type=str)
@click.option("--input_file", required=True, type=str)
def predict_face(face_detector_name, input_file):
    face_detector = get_face_detector(face_detector_name)
    ops.face_detection(face_detector, input_file)


@click.group()
def cli():
    pass


if __name__ == '__main__':
    cli.add_command(predict_face)
    cli.add_command(predict_landmark)
    cli.add_command(track_landmark)
    cli.add_command(track_landmark_camera)
    cli()
