## Overview: 
1. Onnx model
2. Face Detection: 
   - Receptive Field Block Model <mark>(rfb_face_detector)</mark>
   - Light And Fast Face Model <mark>(lffd_face_detector)</mark>
3. Facial Landmark
   - Simple CNN (points(68)-resolution(56)) <mark>(cnn_lms_detector)</mark>
   - Simple CNN (points(68)-resolution(112)) <mark>(high_res_lms_detector)</mark>
   - Simple CNN (points(98)-resolution(112)) <mark>(pfld_lms_detector)</mark>
4. Face Tracking
    - Sort tracker
   
5. Head Pose

## Detail:
1. Model arch:
2. Algorithms 
   
## Usage
1. Face Detection
    ```
   # face_detector (lffd_face_detector)
   python3 main.py --face_detector_name lffd_face_detector predict-face --input_file data/image/img_0.jpg
   
   # face_detector (rfb_face_detector) 
   python3 main.py --face_detector_name rfb_face_detector predict-face --input_file data/image/img_0.jpg
    ```

2. Facial Landmark Detection:
   ```
   # face_detector (lffd_face_detector), lms_detector cnn_lms_detector
   python3 main.py --face_detector_name lffd_face_detector --lms_detector_name cnn_lms_detector predict-landmark --input_file data/image/img_0.jpg 
   
   # face_detector (lffd_face_detector), lms_detector high_res_lms_detector
   python3 main.py --face_detector_name lffd_face_detector --lms_detector_name high_res_lms_detector predict-landmark --input_file data/image/img_0.jpg 
   
   # face_detector (lffd_face_detector), lms_detector pfld_lms_detector
   python3 main.py --face_detector_name lffd_face_detector --lms_detector_name pfld_lms_detector predict-landmark --input_file data/image/img_0.jpg 
    ```
3. Facial Landmark Tracking   
   ```
   # Image
   python3 main.py predict-landmark --face_detector_name lffd_face_detector --lms_detector_name pfld_lms_detector --input_file data/image/img_0.jpg
   
   # Video
   python3 main.py track-landmark --face_detector_name lffd_face_detector --lms_detector_name pfld_lms_detector --input_file data/video/video_0.mp4 --max_faces -1 --is_online True --is_show False
   
   # Video Camera
   python3 main.py track-landmark-camera --face_detector_name lffd_face_detector --lms_detector_name pfld_lms_detector --max_faces -1
   ```
   
## Future work
- Model Improvement (multi-task, high resolution, multi points tracking, model pruning)
- C/C++ implementation
- Mobile device 

## author
1. tuanhleo10@gmail.com
2. anh.lt150071@sis.hust.edu.vn