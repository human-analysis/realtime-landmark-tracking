import time
import unittest

import cv2
import mediapipe as mp

from libs.model import get_face_detector, get_lms_detector
from libs.utils.head_pose import EulerAngles
from libs.utils.visualization import draw_boxes


class LandmarkTester(unittest.TestCase):
    def test_landmark_pipe(self):
        self.temp = None
        mp_draw = mp.solutions.drawing_utils
        mp_face_mesh = mp.solutions.face_mesh
        face_mesh = mp_face_mesh.FaceMesh(max_num_faces=2)
        draw_spec = mp_draw.DrawingSpec(thickness=1, circle_radius=2)

        img = cv2.imread("data/image/img_1.jpg")
        img_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        ih, iw, ic = img.shape
        start = time.time()
        results = face_mesh.process(img_rgb)
        print(time.time() - start)

        print(len(results.multi_face_landmarks))

    def test_face_detection(self):
        self.temp = None
        face_detector = get_face_detector("lffd_face_detector")
        img = cv2.imread("data/image/img_0.jpg")
        start = time.time()
        boxes, scores = face_detector(img)
        print(time.time() - start)

        if True:
            cv2.imshow("image", draw_boxes(img, boxes, scores))
            cv2.waitKey(0)

    def test_landmark_detection(self):
        self.temp = None
        face_detector = get_face_detector("lffd_face_detector")
        lms_detector = get_lms_detector("pfld_lms_detector")
        img = cv2.imread("data/image/img_0.jpg")

        start = time.time()
        boxes, scores = face_detector(img)
        print(f"Detection time: {time.time() - start}")

        # start = time.time()
        facial_lms = lms_detector(img, boxes, scores)
        print(f"Landmark time: {time.time() - start}")

        if False:
            cv2.imshow("img", draw_boxes(img, boxes, scores, facial_lms))
            cv2.waitKey(0)

    def test_landmark_pose(self):
        self.temp = None

        euler_estimator = EulerAngles()
        face_detector = get_face_detector("lffd_face_detector")
        lms_detector = get_lms_detector("pfld_lms_detector")

        img = cv2.imread("data/image/img_0.jpg")
        start = time.time()
        boxes, scores = face_detector(img)
        print("detection: ", time.time() - start)

        start = time.time()
        facial_lms, original_lms, extended_boxes = lms_detector(img, boxes, scores, True)
        print("lms detection: ", time.time() - start)

        head_poses = []
        for box, lms in zip(extended_boxes, original_lms):
            w, h = box[2] - box[0], box[3] - box[1]
            lms = lms.reshape(lms_detector.num_points, 2)
            _, _, euler_angles = euler_estimator.euler_angles_from_landmarks(lms * lms_detector.resolution)
            vis_rvec, vis_tvec, _ = euler_estimator.euler_angles_from_landmarks(lms * (w, h))
            head_poses.append((vis_rvec, vis_tvec, euler_angles))

        img = draw_boxes(img, boxes, scores, facial_lms, head_poses)
        if True:
            cv2.imshow("img", img)
            cv2.waitKey(0)
